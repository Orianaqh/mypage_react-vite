# Mein Portfolio

Die habe ich im April.2022 nur mit html und css/scss gebaut.
Eine simple page ist immer eine gute Übung, deswegen mache ich sie noch ein mal aber jetzt mit React.js.

---

![image](https://user-images.githubusercontent.com/92849517/211198646-ac1d6acb-db03-49a9-8689-0dae3b61eb67.png)

## Packages

-npm create vite@latest
-npm i sass

-npm i react-router-dom
-npm install react-icons

-npm install --save @progress/kendo-react-animation @progress/kendo-licensing

<!-- -npm i animate.css
-npm i loaders.css
-npm i react-loaders

-npm i @emailjs/browser
-npm i @fortawesome/free-brands-svg-icons
-npm i @fortawesome/free-solid-svg-icons
-npm i @fortawesome/react-fontawesome -->

---

### Ordner Struktur

-src
-assets
  -fonts
  -images
-components
  -About
  -ExtraSatz
  -Layout
  -NotFound
  -Sidebar
